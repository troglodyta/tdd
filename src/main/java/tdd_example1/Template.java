package tdd_example1;

import java.util.HashMap;
import java.util.Map;

public class Template {
	private Map<String, String> variables;
	private String templateText;
	
	
	public Template(String templateText) {
		variables = new HashMap<String, String>();
		this.templateText = templateText;
	}
	
	public void set(String variable, String value) {
		variables.put(variable, value);
	}
	
	public String evaluate() {
		String result = replaceVariables();
		checkForMissingValues(result);
		return result;
	}
	
	public void checkForMissingValues(String result) {
		if(result.matches(".*\\$\\{.+\\}.*")){
			throw new RuntimeException();
		}
	}
	
	public String replaceVariables(){
		String result = templateText;
		for(String var: variables.keySet()){
			result = result.replaceAll("\\$\\{" +var+ "\\}", variables.get(var));
		};	
		return result;
	}
}
