package harry_potter_example;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

public class OrderBasket {
	private List<Book> books = new ArrayList<>();
	private static final double BOOK_PRICE = 8.0;
	private static final double TWO_BOOK_DISC = 0.95;
	private static final double THREE_BOOK_DISC = 0.9;
	private static final double FOUR_BOOK_DISC = 0.8;
	private static final double FIVE_BOOK_DISC = 0.7;

	public void add(Book book) {
		books.add(book);
	}

	public Double getFinalPrice() throws OperationNotSupportedException {
		throw new OperationNotSupportedException("Not implemented yet");
		
	}
}
