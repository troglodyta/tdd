package ttd_example_mock2;

public class Restaurant {
	private RestaurantValidator validator;
	private RestaurantDAO restaurantDAO;
	
	public void setValidator(RestaurantValidator validator) {
		this.validator = validator;
	}
	
	public void setRestaurantDAO(RestaurantDAO restaurantDAO) {
		this.restaurantDAO = restaurantDAO;
	}
	
	public Restaurant createRestaurant( Restaurant restaurant ) {
		if(validator.validateRestaurant(restaurant ) && restaurantDAO.persist(restaurant)) {
				return restaurant;
		}
		return null;
	}

}
