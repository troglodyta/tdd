package tdd_example_tests1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd_example1.Template;

public class TestTempate {

	private Template template;
	
	@Before
	public void setUp() throws Exception {
		template = new Template("${one}, ${two}, ${three}");
		template.set("one", "1");
		template.set("two", "2");
		template.set("three", "3");
	}
	
	@Test
	public void differentTemplate() throws Exception {
		Template template2 = new Template("Hi, ${name}");
		template2.set("name", "someone else");
		assertEquals("Hi, someone else", template2.evaluate());
	}
	
	@Test
	public void multipleVariables() throws Exception {
		assertEquals("1, 2, 3", template.evaluate());
	}
	
	@Test
	public void unknownVariablesAreIgnored() throws Exception {
		template.set("notexists", "Hi");
		assertEquals("1, 2, 3", template.evaluate());
	}
	
	@Test
	public void missingValueRiseException() throws Exception {
		Template newTemplate = new Template("Hi ${foo}");
		try{
			newTemplate.evaluate();
			fail("When value is missing, then exception should throw");
		}
		catch(RuntimeException e) {	
		}
	}
	

}
