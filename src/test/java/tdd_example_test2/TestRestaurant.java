package tdd_example_test2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ttd_example_mock2.Restaurant;
import ttd_example_mock2.RestaurantDAO;
import ttd_example_mock2.RestaurantValidator;

public class TestRestaurant {
	
	RestaurantValidator restValMock;
	RestaurantDAO restDAOMock;
	Restaurant restaurant;
	
	@Before
	public void setUp() throws Exception {		
		 restValMock = Mockito.mock(RestaurantValidator.class);
		 restDAOMock = Mockito.mock(RestaurantDAO.class);
		 restaurant = new Restaurant();
		 restaurant.setValidator(restValMock);
		 restaurant.setRestaurantDAO(restDAOMock);
	}
	
	@Test
	public void restaurantCreated() {
		Mockito.when(restValMock.validateRestaurant(restaurant)).thenReturn(true);
		Mockito.when(restDAOMock.persist(restaurant)).thenReturn(true);
		
		
		Restaurant newRestaurant = restaurant.createRestaurant(restaurant);
		
		Mockito.verify(restValMock, Mockito.times(1)).validateRestaurant(restaurant);
		Mockito.verify(restDAOMock, Mockito.times(1)).persist(restaurant);
		assertNotNull(newRestaurant);
	}
	
	@Test
	public void restaurantNotCreatedPersistFailure() {
		Mockito.when(restValMock.validateRestaurant(restaurant)).thenReturn(true);
		Mockito.when(restDAOMock.persist(restaurant)).thenReturn(false);
		
		Restaurant newRestaurant = restaurant.createRestaurant(restaurant);
		
		Mockito.verify(restValMock,Mockito.times(1)).validateRestaurant(restaurant);
		Mockito.verify(restDAOMock,Mockito.times(1)).persist(restaurant);
		
		assertNull(newRestaurant);
	}
	

	@Test
	public void restaurantNotCreatedValidationFailure() {
		Mockito.when(restValMock.validateRestaurant(restaurant)).thenReturn(false);
		Mockito.when(restDAOMock.persist(restaurant)).thenReturn(false);
		
		Restaurant newRestaurant = restaurant.createRestaurant(restaurant);
		
		Mockito.verify(restValMock, Mockito.times(1)).validateRestaurant(restaurant);
		Mockito.verify(restDAOMock,Mockito.times(0)).persist(restaurant);
		assertNull(newRestaurant);
	}
	
	

}
